public enum VehicleType
{
    Motocycle = 1,
    Car,
    Bus,
    Lorry
}
public class Vehicle
{
    public string LicensePlates { get; }
    public string Make { get; }
    public double Deposit { get; set; }
    public double Payment { get; set; }
    public VehicleType VehicleType { get; }

    public Vehicle(string licensePlates, string make, double deposit, VehicleType vehicleType)
    {
        LicensePlates = licensePlates;
        Make = make;
        Deposit = deposit;
        VehicleType = vehicleType;
        Payment = 0;
    }

}