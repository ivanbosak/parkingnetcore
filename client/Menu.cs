using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace client
{
    static class Menu
    {
        public static void PrintMainHeader()
        {
            Console.WriteLine("Welcome to our Parking...");
            Console.WriteLine("If you are a cutomer press - 1;");
            Console.WriteLine("If you are an administrator press - 2;");
        }
        public static void PrintCustomerHeader()
        {
            Console.WriteLine("Please choose action...");
            Console.WriteLine("Park vehicle - 1, Take back vehicle - 2, Get amount of places - 3, Refill deposit - 4");
            Console.WriteLine("To retrun back press 0");
            Console.WriteLine("Enter number of the action:");
        }
        public static void PrintAdministratorHeader()
        {
            Console.WriteLine("Please choose action...");
            Console.WriteLine("Print all vehicles - 1, Print transactions for minute - 2, Print all transactions - 3");
            Console.WriteLine("Print the amount of money earned in the last minute - 4, Print current balance of parking - 5");
            Console.WriteLine("To retrun back press 0");
            Console.WriteLine("Enter number of the action:");
        }

        public async static Task ParkVehicle(ParkingClient client)
        {
            if ((int)await client.InvokeApiAsync("api/parking/freeplaces") == 0)
            {
                Console.WriteLine("There no free places on the parking.");
                Console.ReadLine();
                Console.Clear();
                return;
            }

            int category;
            double deposit;
            bool parsingResult;
            string licensePlates;
            string make;
            VehicleType vehicleType;

            while (true)
            {
                Console.WriteLine("Please choose category of your vehicle...");
                Console.WriteLine("Motocycle - 1, Car - 2, Bus - 3, Lorry - 4");
                Console.WriteLine("Enter category of your vehicle:");

                parsingResult = Int32.TryParse(Console.ReadLine(), out category);

                if (parsingResult && category != 0 && category < 5)
                {
                    vehicleType = (VehicleType)category;
                    break;
                }
            }

            while (true)
            {
                Console.WriteLine("Enter license plates of your vehicle:");
                licensePlates = Console.ReadLine();
                
                var vehicle = await client.GetSingleVehicleAsync(licensePlates);

                if (vehicle == null)
                    break;

                Console.WriteLine("Vehicle with license plates {0} is already parked.", licensePlates);
            }

            Console.WriteLine("Enter make of your vehicle:");
            make = Console.ReadLine();

            while (true)
            {
                Console.WriteLine("Enter initial deposit:");

                if (Double.TryParse(Console.ReadLine(), out deposit))
                    break;
            }

           await client.CreateVehicleAsync(new Vehicle(licensePlates, make, deposit, vehicleType));

            Console.WriteLine("Your vehicle has been parked successfully!\n");
            Console.ReadLine();
            Console.Clear();
        }

    public async static Task TekeBackVehicle(ParkingClient client)
    {
            string licensePlates;

            if ((int)await client.InvokeApiAsync("api/parking/occupiedplaces") == 0)
            {
                Console.WriteLine("There no vehicles on the parking.");
                Console.ReadLine();
                Console.Clear();
                return;
            }

            while (true)
            {
                Console.WriteLine("Enter license plates of your vehicle:");
                licensePlates = Console.ReadLine();

                var vehicleToRemove = await client.GetSingleVehicleAsync(licensePlates);

                if (vehicleToRemove != null)
                {
                    if (vehicleToRemove.Payment != 0)
                    {
                        double payment;
                        while (true)
                        {
                            Console.WriteLine("You should pay extra {0} dollars to take back your vehicle...", vehicleToRemove.Payment);
                            Console.WriteLine("Enter money:");

                            if (Double.TryParse(Console.ReadLine(), out payment))
                            {
                                if (payment < vehicleToRemove.Payment)
                                {
                                    vehicleToRemove.Payment = vehicleToRemove.Payment - payment;
                                    await client.PostApiAsync("api/parking/addbalance", payment);
                                    await client.CreateTransactionAsync(new Transaction(vehicleToRemove.LicensePlates, payment));
                                }
                                else
                                {
                                    vehicleToRemove.Deposit = payment - vehicleToRemove.Payment;
                                    await client.PostApiAsync("api/parking/addbalance", vehicleToRemove.Payment);
                                    await client.CreateTransactionAsync(new Transaction(vehicleToRemove.LicensePlates, vehicleToRemove.Payment));
                                    break;
                                }
                            }
                        }
                    }
                    await client.DeleteProductAsync(vehicleToRemove.LicensePlates);
                    Console.WriteLine("Your vehicle has been removed from parking...");
                    if (vehicleToRemove.Deposit != 0)
                        Console.WriteLine("Take your cash back - {0}", vehicleToRemove.Deposit);
                    break;

                }
                else
                {
                    Console.WriteLine("Vehicle with these license plates is not found");
                }
            }

            Console.ReadLine();
            Console.Clear();
        }

        public async static Task PrintPlaces(ParkingClient client)
        {
            int free = (int)await client.InvokeApiAsync("api/parking/freeplaces");
            int occupied = (int)await client.InvokeApiAsync("api/parking/occupiedplaces");
            Console.WriteLine("On the parking there are {0} free and {1} occupied places.", free, occupied);
            Console.ReadLine();
        }

        public async static Task RefillDeposit(ParkingClient client)
        {
            string licensePlates;
            double deposit;

            if ((int)await client.InvokeApiAsync("api/parking/occupiedplaces") == 0)
            {
                Console.WriteLine("There no vehicles on the parking.");
                Console.ReadLine();
                Console.Clear();
                return;
            }

            Console.WriteLine("Enter license plates of your vehicle:");
            licensePlates = Console.ReadLine();

            var vehicle = await client.GetSingleVehicleAsync(licensePlates);

            if (vehicle != null)
            {
                while (true)
                {
                    Console.WriteLine("Enter deposit:");

                    if (Double.TryParse(Console.ReadLine(), out deposit))
                    {
                        await client.UpdateVehicleAsync(vehicle.LicensePlates, deposit);
                        Console.WriteLine("Your deposit has been reffiled successfully!");
                        Console.WriteLine($"Now your deposit is - {vehicle.Deposit}!");
                        break;
                    }
                }
            }
            else
            {
                Console.WriteLine("Vehicle with these license plates is not found");
            }
            Console.ReadLine();
        }
        public async static Task PrintAllVehicles(ParkingClient client)
        {
            if ((int)await client.InvokeApiAsync("api/parking/occupiedplaces") == 0)
            {
                Console.WriteLine("There no vehicles on the parking.");
            }
            else
            {
                var vehicles = await client.GetVehiclesAsync();
                Console.WriteLine("License Plates      Make      Vehicle Type");
                
                foreach (Vehicle v in vehicles)
                {
                    Console.WriteLine(String.Format("{0,-20}{1,-10}{2,-12}", v.LicensePlates, v.Make, v.VehicleType.ToString()));
                }
            }

            Console.ReadLine();
            Console.Clear();
        }

        public async static Task PrintTransactionsForMinute(ParkingClient client)
        {
            var transactions = await client.GetTransactionsAsync();

            if (transactions == null || transactions.Count() == 0)
            {
                Console.WriteLine("There no transactions for the last minute.");
            }
            else
            {
                Console.WriteLine("License Plates      Make       Vehicle Type");

                foreach (Transaction tr in transactions)
                {
                    Console.WriteLine($"License Plates - {tr.LicensePlates}, withdrawn money - {tr.Withdrawn}");
                }
            }

            Console.ReadLine();
            Console.Clear();
        }

        public async static Task PrintLogFile(ParkingClient client)
        {
            Console.WriteLine(await client.InvokeApiAsync("api/parking/log"));

            Console.ReadLine();
            Console.Clear();
        }

        public async static Task PrintEarned(ParkingClient client)
        {
            Console.WriteLine("For last minute parking earned - {0}", 
                    await client.InvokeApiAsync("api/parking/earned"));

            Console.ReadLine();
            Console.Clear();
        }

        public async static Task PrintBalance(ParkingClient client)
        {
            Console.WriteLine("Balance of parking is - {0}", 
                    await client.InvokeApiAsync("api/parking/balance"));

            Console.ReadLine();
            Console.Clear();
        }
        
    }
}
