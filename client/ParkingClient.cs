using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;


namespace client
{
    public class ParkingClient
    {
        private readonly HttpClient _client;
        private readonly string _apiVehiclePath;
        private readonly string _apiTransactionPath;

        public ParkingClient(HttpClient client, string serverUrl)
        {
            _client = client;
            client.BaseAddress = new Uri(serverUrl);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            _apiVehiclePath = "api/vehicles/";
            _apiTransactionPath = "api/transactions/";
        }

        public async Task<IEnumerable<Vehicle>> GetVehiclesAsync()
        {
            IEnumerable<Vehicle> list = null;

            using(var response = await _client.GetAsync(_apiVehiclePath))
            {    
                response.EnsureSuccessStatusCode();

                if (response.IsSuccessStatusCode)
                {
                    list = await response.Content.ReadAsAsync<IEnumerable<Vehicle>>();
                }
            }
            return list;
        }

        public async Task<IEnumerable<Transaction>> GetTransactionsAsync()
        {
            IEnumerable<Transaction> list = null;

            using(var response = await _client.GetAsync(_apiTransactionPath))
            {    
                response.EnsureSuccessStatusCode();

                if (response.IsSuccessStatusCode)
                {
                    list = await response.Content.ReadAsAsync<IEnumerable<Transaction>>();
                }
            }
            return list;
        }

        public async Task CreateVehicleAsync(Vehicle vehicle)
        {
            using(var  response = await _client.PostAsJsonAsync(_apiVehiclePath, vehicle))
            {
                response.EnsureSuccessStatusCode();
            }
        }

        public async Task CreateTransactionAsync(Transaction transaction)
        {
            using(var  response = await _client.PostAsJsonAsync(_apiTransactionPath, transaction))
            {
                response.EnsureSuccessStatusCode();
            }
        }

        public async Task<Vehicle> GetSingleVehicleAsync(string licensePlates)
        {
            Vehicle vehicle = null;
            using(var  response = await _client.GetAsync(_apiVehiclePath + licensePlates))
            {
                if (response.IsSuccessStatusCode)
                {
                    vehicle = await response.Content.ReadAsAsync<Vehicle>();
                }
            }
            return vehicle;
        }

        public async Task UpdateVehicleAsync(string licensePlates, double sum)
        {
            var response = await _client.PutAsJsonAsync(_apiVehiclePath + $"{licensePlates}/{sum}", 0);
            response.EnsureSuccessStatusCode();
        }

        public async Task DeleteProductAsync(string licensePlates)
        {
            using(var  response = await _client.DeleteAsync(_apiVehiclePath + licensePlates))
            {
                response.EnsureSuccessStatusCode();
            }
        }

        public async Task<dynamic> InvokeApiAsync(string api)
        {
            dynamic result = 0;
            using(var  response = await _client.GetAsync(api))
            {
                response.EnsureSuccessStatusCode();

                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<dynamic>();
                }
            }
            return result;
        }
        public async Task PostApiAsync(string api, double sum)
        {
            using(var  response = await _client.PostAsJsonAsync(api, sum))
            {
                response.EnsureSuccessStatusCode();
            }
        }

    }
}