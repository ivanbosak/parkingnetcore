public class Transaction
{
    public string LicensePlates { get; set; }
    public double Withdrawn { get; set; }
    public Transaction(string licensePlates, double withdrawn)
    {
         LicensePlates = licensePlates;
          Withdrawn = withdrawn;
    }
}