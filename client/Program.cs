﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace client
{
    class Program
    {
        static HttpClient client = new HttpClient();

        static void Main(string[] args)
        {
            int action;
            bool parsingResult;

            var parkingClient = new ParkingClient(client, "http://localhost:5000/");

            while (true)
            {
                Menu.PrintMainHeader();

                parsingResult = Int32.TryParse(Console.ReadLine(), out action);

                if (parsingResult == true)
                {

                    if (action == 1)
                    {
                        while (true)
                        {

                            Menu.PrintCustomerHeader();

                            parsingResult = Int32.TryParse(Console.ReadLine(), out action);

                            try
                            {
                                if (parsingResult == true)
                                {
                                    switch (action)
                                    {
                                        case 0:
                                            break;
                                        case 1:
                                            Menu.ParkVehicle(parkingClient).Wait();
                                            break;
                                        case 2:
                                            Menu.TekeBackVehicle(parkingClient).Wait();
                                            break;
                                        case 3:
                                            Menu.PrintPlaces(parkingClient).Wait();
                                            break;
                                        case 4:
                                            Menu.RefillDeposit(parkingClient).Wait();
                                            break;
                                        default:
                                            continue;
                                    }
                                    break;
                                }
                            }   
                            catch(HttpRequestException e)
                            {
                                Console.WriteLine("An error occurred while connecting to server" + e.Message);
                            }
                            catch(Exception e)
                            {
                                Console.WriteLine("Unexpected exception" + e.Message);
                            }
                        }
                    }
                    else if (action == 2)
                    {
                        while (true)
                        {

                            Menu.PrintAdministratorHeader();

                            parsingResult = Int32.TryParse(Console.ReadLine(), out action);
                            try
                            {
                                if (parsingResult == true)
                                {
                                    switch (action)
                                    {
                                        case 0:
                                            break;
                                        case 1:
                                            Menu.PrintAllVehicles(parkingClient).Wait();
                                            break;
                                        case 2:
                                            Menu.PrintTransactionsForMinute(parkingClient).Wait();
                                            break;
                                        case 3:
                                            Menu.PrintLogFile(parkingClient).Wait();
                                            break;
                                        case 4:
                                            Menu.PrintEarned(parkingClient).Wait();
                                            break;
                                        case 5:
                                            Menu.PrintBalance(parkingClient).Wait();
                                            break;
                                        default:
                                            continue;
                                    }
                                    break;
                                }
                            }
                            catch(HttpRequestException e)
                            {
                                Console.WriteLine("An error occurred while connecting to server" + e.Message);
                            }
                            catch(Exception e)
                            {
                                Console.WriteLine("Unexpected exception" + e.Message);
                            }
                        }

                    }
                    Console.Clear();
                }
            }
        }
    }
}

