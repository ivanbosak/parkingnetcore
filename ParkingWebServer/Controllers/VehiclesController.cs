using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ParkingWebServer.Models;
using ParkingWebServer.Services;

namespace ParkingWebServer.Controllers
{
 [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly ParkService _parkingService;

        public VehiclesController(ParkService service)
        {
            _parkingService = service;
        }
        // GET api/vehicles
        [HttpGet]
        public IEnumerable<Vehicle> GetVehicles()
        {
            return _parkingService.GetAllVehicles();
        }

        // GET api/vehicles/id
        [HttpGet("{id}")]
        public ActionResult<Vehicle> Get(string id)
        {
            var vehicle = _parkingService.FindVehicle(id);

            if(vehicle == null)
                return NotFound();

            return Ok(vehicle);
        }

        // POST api/vehicles/
        [HttpPost]
        public ActionResult Post([FromBody]Vehicle vehicle)
        {
            if(!ModelState.IsValid)
                return BadRequest();

            _parkingService.AddVehicle(vehicle);
            return Ok();
        }

        // PUT api/vehicles/id
        [HttpPut("{id}/{sum}")]
        public void PutDeposit(string id, double sum)
        {
           var vehicle = _parkingService.FindVehicle(id);
           vehicle.Deposit += sum;

        }

        // DELETE api/vehicles/id
        [HttpDelete("{id}")]
        public ActionResult DeleteVehicle(string id)
        {
            var vehicle = _parkingService.FindVehicle(id);

            if(vehicle == null)
                return NotFound();

            _parkingService.RemoveVehicle(vehicle);

            return Ok();
        }
    }
}