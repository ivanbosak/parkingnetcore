using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ParkingWebServer.Models;
using ParkingWebServer.Services;

namespace ParkingWebServer.Controllers
{
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly ParkService _parkingService;

        public ParkingController(ParkService service)
        {
            _parkingService = service;
        }
        [HttpGet]
        [Route("api/parking/freeplaces")]
        public ActionResult<int> GetFreePlaces()
        {
            int number = _parkingService.GetNumberOfFreePlaces();
            return Ok(number);
        }

        [HttpGet]
        [Route("api/parking/occupiedplaces")]
        public ActionResult<int> GetOccupiedPlaces()
        {
            int number = _parkingService.GetNumberOfOccupiedPlaces();
            return Ok(number);
        }
        [HttpGet]
        [Route("api/parking/balance")]
        public ActionResult<double> GetBalnce()
        {
            double sum = _parkingService.GetBalance();
            return Ok(sum);
        }

        [HttpGet]
        [Route("api/parking/earned")]
        public ActionResult<double> GetEarnedForMinute()
        {
            double sum = _parkingService.CalculateEarnedForMinute();
            return Ok(sum);
        }

        [HttpGet]
        [Route("api/parking/log")]
        public ActionResult<string> GetTransactionsLog()
        {
            string log = _parkingService.ReadLogFile();
            return Ok(log);
        }

        // POST api/transactions/
        [HttpPost]
        [Route("api/parking/addbalance")]
        public void  Post(double sum)
        {
            _parkingService.AddBalance(sum);
        }
    }
}