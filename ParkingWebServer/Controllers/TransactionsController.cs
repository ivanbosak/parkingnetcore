using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ParkingWebServer.Models;
using ParkingWebServer.Services;

namespace ParkingWebServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController: ControllerBase
    {
        private readonly ParkService _parkingService;

        public TransactionsController(ParkService service)
        {
            _parkingService = service;
        }
        // GET api/transactions
        [HttpGet]
        public IEnumerable<Transaction> GetTransactionsForMinute()
        {
            return _parkingService.GetAllTransactions();
        }

        // POST api/transactions/
        [HttpPost]
        public ActionResult Post([FromBody]Transaction transaction)
        {
            if(!ModelState.IsValid)
                return BadRequest();

            _parkingService.AddTransaction(transaction);
            return Ok();
        }
        
    }
}