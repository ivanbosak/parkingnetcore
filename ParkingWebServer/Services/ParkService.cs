using System;
using System.Collections.Generic;
using System.Linq;
using ParkingWebServer.Models;

namespace ParkingWebServer.Services
{
    public class ParkService
    {
        private readonly List<Vehicle> _vehicles = new List<Vehicle>();
        private readonly List<Transaction> _transactions = new List<Transaction>();
        private static TransactionHandler _transactionHandler;
        private double _balance;

        public ParkService()
        {
            _balance = Properties.Balance;
            _transactionHandler = new TransactionHandler(new Logger("Transactions.txt"), _vehicles, _transactions, this);
            _transactionHandler.RunHandler();
        }

        public string ReadLogFile()
        {
            return _transactionHandler.ReadLog();
        }
        public double GetBalance()
        {
            return _balance;
        }

        public void AddBalance(double sum)
        {
            _balance = _balance + sum;
        }

        public int GetNumberOfFreePlaces()
        {
            return Properties.MaxParkingPlaces - _vehicles.Count;
        }
        public int GetNumberOfOccupiedPlaces()
        {
            return _vehicles.Count;
        }
        public void AddTransaction(Transaction transaction)
        {
            _transactions.Add(transaction);
        }
        public IEnumerable<Transaction> GetAllTransactions()
        {
            return _transactions;
        }
        public double CalculateEarnedForMinute()
        {
            double sum = 0;

            foreach(Transaction tr in _transactions)
            {
                sum += tr.Withdrawn;
            }
            return sum;
        }
        public void AddVehicle(Vehicle vehicle)
        {
            _vehicles.Add(vehicle);
        }

        public bool RemoveVehicle(Vehicle vehicle)
        {
            return _vehicles.Remove(vehicle);
        }
        public Vehicle FindVehicle(string licensePlates)
        {
            return _vehicles.FirstOrDefault(v => v.LicensePlates
                            .Equals(licensePlates, StringComparison.OrdinalIgnoreCase));
        }
        public IEnumerable<Vehicle> GetAllVehicles()
        {
            return _vehicles;
        }
    }
}