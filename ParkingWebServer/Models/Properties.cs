namespace ParkingWebServer.Models
{
    static class Properties
    {
        public static double Balance { get; set; } = 0;
        public static int MaxParkingPlaces { get; set; } = 10;
        public static int IntervalInSeconds { get; set; } = 5;
        public static double CoefficientOfFine { get; set; } = 2.5;
        public static double MotocycleFee { get; set; } = 1;
        public static double CarFee { get; set; } = 2;
        public static double BusFee { get; set; } = 3.5;
        public static double LorryFee { get; set; } = 5;
    }
}