namespace ParkingWebServer.Models
{
    public interface ILogger
    {
        void Log(string str);
        string ReadFile();
    }
}