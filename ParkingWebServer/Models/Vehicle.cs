using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace ParkingWebServer.Models
{
     public enum VehicleType
    {
        Motocycle = 1,
        Car,
        Bus,
        Lorry
    }

    public class Vehicle
    {
        public string LicensePlates { get; }
        public string Make { get; }
        public double Deposit { get; set; }
        public double Payment { get; set; }
        [Range(1, 4)]
        public VehicleType VehicleType { get; }

        public Vehicle(string licensePlates, string make, double deposit, VehicleType vehicleType)
        {
            LicensePlates = licensePlates;
            Make = make;
            Deposit = deposit;
            VehicleType = vehicleType;
            Payment = 0;
        }
        public void AddToDeposit(double sum)
        {
            Deposit = Deposit + sum;
        }

    }
}