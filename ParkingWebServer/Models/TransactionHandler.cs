using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using ParkingWebServer.Services;

namespace ParkingWebServer.Models
{
    public class TransactionHandler
    {
        private Timer withdrawnTimer;
        private Timer logTimer;

        private readonly IList<Vehicle> _vehicleList;
        private readonly IList<Transaction> _transactionList;
        private readonly ParkService _parking;

        private readonly ILogger _logger;

        private const int OneSecondInMS = 1000;
        public TransactionHandler(ILogger logger, IList<Vehicle> vehicleList,
            IList<Transaction> transactionList, ParkService parkService)
        {
            _logger = logger;
            _vehicleList = vehicleList;
            _transactionList = transactionList;
            _parking = parkService;
        }

        public void RunHandler()
        {
            withdrawnTimer = new Timer(Properties.IntervalInSeconds * OneSecondInMS);
            withdrawnTimer.Elapsed += WithdrawnTimerElapsed;
            withdrawnTimer.Start();

            logTimer = new Timer(60 * OneSecondInMS);
            logTimer.Elapsed += LogTimerElapsed;
            logTimer.Start();
        }

        public void StopHandler()
        {
            withdrawnTimer.Stop();

            logTimer.Stop();
        }

        public string ReadLog()
        {
            return _logger.ReadFile();
        }

        private void WithdrawnTimerElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //There no vehicles to charge fee
            if (_vehicleList.Count == 0)
                return;
            
            double charge;

            foreach(Vehicle v in _vehicleList)
            {
                charge = CalculateCharge(v);

                //When amount of money is not enough to pay
                if(charge > v.Deposit)
                {
                    //Get all money from deposit and calculate fine with coefficient
                    charge = charge - v.Deposit;

                    if (v.Deposit > 0)
                    {
                        _parking.AddBalance(v.Deposit);
                        _transactionList.Add(new Transaction(v.LicensePlates, v.Deposit));
                        v.Deposit = 0;
                    }

                    v.Payment = v.Payment + charge * Properties.CoefficientOfFine;
                }
                else
                {
                    v.Deposit = v.Deposit - charge;
                    _parking.AddBalance(charge);
                    _transactionList.Add(new Transaction(v.LicensePlates, charge));
                }
            }

        }

        private void LogTimerElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (_transactionList.Count != 0)
            {
                for (int i = 0; i < _transactionList.Count; i++)
                {
                    var tr = _transactionList[i];
                    if(tr != null)
                        _logger.Log($"License Plates - {tr.LicensePlates}, withdrawn money - {tr.Withdrawn}");
                }

                _transactionList.Clear();
            }
        }

        private double CalculateCharge(Vehicle vehicle)
        {
            double charge = 0;

            switch (vehicle.VehicleType)
            {
                case VehicleType.Motocycle:
                    charge = Properties.MotocycleFee;
                    break;
                case VehicleType.Car:
                    charge = Properties.CarFee;
                    break;
                case VehicleType.Bus:
                    charge = Properties.BusFee;
                    break;
                case VehicleType.Lorry:
                    charge = Properties.LorryFee;
                    break;

            }

            return charge;
        }
    }
}