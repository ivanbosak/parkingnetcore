using System;
using System.IO;

namespace ParkingWebServer.Models
{
    public class Logger: ILogger
    {
        private readonly string _fileName;
        public Logger(string fileName)
        {
            _fileName = fileName;
        }
        public void Log(string str)
        {
            try
            {
                using (StreamWriter outputFile = new StreamWriter(File.Open( _fileName, System.IO.FileMode.Append)))
                {
                    outputFile.WriteLine(str);
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("The file could not be written:");
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Unexpected error occured during writing of the file:");
                Console.WriteLine(e.Message);
            }
        }

        public string ReadFile()
        {
            string line = string.Empty;

            try
            { 
                using (StreamReader sr = new StreamReader(_fileName))
                {
                    line = sr.ReadToEnd();
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Unexpected error occured during reading of the file:");
                Console.WriteLine(e.Message);
            }

            return line;
        }
    }
}